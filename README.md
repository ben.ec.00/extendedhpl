# ExtendedHPL

This is the Mathematica package ExtendedHPL, discussed in the Bachelorthesis "Properties of iterated integrals -  an implementation in Mathematica".
The second file "ValueTable.wl" has to be in the same folder as the primary file "ExtendedHPL.wl".
A documentation can be found within the Bachelorthesis.
For questions, please contact Ben Eckardt.
