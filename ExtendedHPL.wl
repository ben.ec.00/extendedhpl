(* ::Package:: *)

BeginPackage["ExtendedHPL`"];


(* ::Subsection:: *)
(*Usage*)


ExtHPL::usage = ToString[#, StandardForm]&/@{
	"ExtHPL[{", Style["__String", "TI"], "}, ", Style["x", "TI"], "]",
	" gives the harmonic polylogarithm. \n", 
	"All implemented letters are \"0\", \"1\", \"-1\", \"{4,0}\" and \"{4,1}\"."
}//StringJoin;


(*
ExtHPLLetter::usage = "/!ExtHPLLetter[a,x] gives the corresponding letter f(x)\n
ExtHPLLetter[\"0\",x]=1/x
ExtHPLLetter[\"1\",x]=1/(1-x)
ExtHPLLetter[\"-1\",x]=1/(1+x)
ExtHPLLetter[\"{4,0}\",x]=1/(1+x^2)
ExtHPLLetter[\"{4,1}\",x]=x/(1+x^2)"
*)
ExtHPLLetter::usage = ToString[#, StandardForm]&/@{
	"ExtHPLLetter[", Style["b_String", "TI"], ", ", Style["x", "TI"], "]",
	" gives the functional form of the letter b. \n", 
	"All implemented letters are \"0\", \"1\", \"-1\", \"{4,0}\" and \"{4,1}\"."
}//StringJoin;


(*LeibnizRule::usage = "LeibnizRule[a, operator] is an implementation of the Leibniz Rule for an operator applied to the List a."*)


(*
ExtHPLDerivative::usage = "ExtHPLDerivative[exp,x] gives the derivative of exp with respect to x.
It can differentiate arrays/lists, sums and products of HPLs. For expressions without any HPLs, the native command D[exp,x] is used."
*)
ExtHPLDerivative::usage = ToString[#, StandardForm]&/@{
	"ExtHPLDerivative[", Style["exp", "TI"], ", ", Style["x", "TI"], "]",
	" gives the derivative of ", Style["exp", "TI"], " with respect to ", Style["x", "TI"], ". \n", 
	"It can differentiate ", Hyperlink["lists", "paclet:ref/List"], ", ", Hyperlink["sums", "paclet:ref/Sum"], " and ", Hyperlink["products", "paclet:ref/Product"], " of HPLs. \n", 
	"For expressions without any HPLs, the native command ", Hyperlink["D[exp,x]", "paclet:ref/D"], " is used."
}//StringJoin;


(*
ExtHPLRemoveTrailingLetters::usage "ExtHPLRemoveTrailingZeros[ExtHPL[a,x],b] uses multiplication identities to rewrite HPLs with trailing b's as other HPLs with the explicit logarithmic dependency.
The Option \"AutoConvertToKnownFunctions\" controls, if HPLs of weight 1 should be rewritten into elementary functions.
If applied to a general expression, all HPLs inside the expression will be rewritten separately.
Computations for more than 5 trailing b's will be slow."
ExtHPLRemoveLeadingLetters::usage "ExtHPLRemoveLeadingZeros[ExtHPL[a,x],b] uses multiplication identities to rewrite HPLs with leading b's as other HPLs with the explicit logarithmic dependency.
The Option \"AutoConvertToKnownFunctions\" controls, if HPLs of weight 1 should be rewritten into elementary functions.
If applied to a general expression, all HPLs inside the expression will be rewritten separately.
Computations for more than 5 leading b's will be slow."
*)
ExtHPLRemoveTrailingLetters::usage = ToString[#, StandardForm]&/@{
	"ExtHPLRemoveTrailingZeros[ExtHPL[", Style["a", "TI"], ", ", Style["x", "TI"], "], ", Style["b", "TI"], "]",
	" uses multiplication identities to rewrite ExtHPL[", Style["a", "TI"], ", ", Style["x", "TI"], "] with trailing ", Style["b", "TI"], "'s as other HPLs with the explicit logarithmic dependency. \n", 
	"The Option ", Style["\"AutoConvertToKnownFunctions\"","TI"], " controls, if HPLs of weight 1 should be rewritten into elementary functions. \n", 
	"If applied to a general expression, all HPLs inside the expression will be rewritten separately. \n", 
	"Computations for more than 5 trailing ", Style["b","TI"], "'s will be slow."
}//StringJoin;
ExtHPLRemoveLeadingLetters::usage = ToString[#, StandardForm]&/@{
	"ExtHPLRemoveLeadingZeros[ExtHPL[", Style["a", "TI"], ", ", Style["x", "TI"], "], ", Style["b", "TI"], "]",
	" uses multiplication identities to rewrite ExtHPL[", Style["a", "TI"], ", ", Style["x", "TI"], "] with leading ", Style["b", "TI"], "'s as other HPLs with the explicit logarithmic dependency. \n", 
	"The Option ", Style["\"AutoConvertToKnownFunctions\"","TI"], " controls, if HPLs of weight 1 should be rewritten into elementary functions. \n", 
	"If applied to a general expression, all HPLs inside the expression will be rewritten separately. \n", 
	"Computations for more than 5 leading ", Style["b","TI"], "'s will be slow."
}//StringJoin;


(*
ExtHPLSeries::usage = "ExtHPLSeries[ExtHPL[a,x],{x,0,n}] expands the HPL around zero up to n-th order.
It can handle sums of HPLs and an expression multiplied by a HPL. Expressions without HPLs utilize the native command Series[exp,{x,0,n}] is utilized.
Computations for more than 5 trailing zeros will be slow."
ExtHPLSeriesCoefficient::usage = "ExtHPLSeriesCoefficient[ExtHPL[a,x],{x,0,n}] gives the n-th order coefficient of an expansion of the HPL around 0.
It can handle sums of HPLs, but, unlike the ExtHPLSeries commmand, cannot handle an expression multiplied by a HPL. Expressions without HPLs utilize the native command SeriesCoefficienz[exp,{x,0,n}] is utilized.
Computations for more than 5 trailing zeros will be slow."
*)
ExtHPLSeries::usage = ToString[#, StandardForm]&/@{
	"ExtHPLSeries[ExtHPL[", Style["a", "TI"], ", ", Style["x", "TI"], "], {", Style["x", "TI"], ", 0, ", Style["n", "TI"], "}]",
	" expands the HPL around 0 up to ", Style["n", "TI"], "-th order. \n", 
	"It can handle ", Hyperlink["sums", "paclet:ref/Sum"], " of HPLs and an expression multiplied by a HPL. \n", 
	"Expressions without HPLs utilize the native command ", Hyperlink["Series[exp,{x,0,n}]", "paclet:ref/Series"], " is utilized."
}//StringJoin;
ExtHPLSeriesCoefficient::usage = ToString[#, StandardForm]&/@{
	"ExtHPLSeries[ExtHPL[", Style["a", "TI"], ", ", Style["x", "TI"], "], {", Style["x", "TI"], ", 0, ", Style["n", "TI"], "}]",
	" expands the HPL around 0 up to ", Style["n", "TI"], "-th order. \n", 
	"It can handle ", Hyperlink["sums", "paclet:ref/Sum"], " of HPLs, but, unlike the ExtHPLSeries command, cannot handle an expression multiplied by a HPL. \n", 
	"Expressions without HPLs utilize the native command ", Hyperlink["SeriesCoefficient[exp,{x,0,n}]", "paclet:ref/SeriesCoefficient"], " is utilized."
}//StringJoin;


(*
ExtHPLIntegrate::usage = "ExtHPLIntegrate[f(x)*ExtHPL[{...},x],{x,0,y}] decomposes f(x) into HPLLetters and uses integration by parts and the definition of HPLs to get HPLs with a bigger weight."
*)
ExtHPLIntegrate::usage = ToString[#, StandardForm]&/@{
	"ExtHPLIntegrate[", Style["exp", "TI"], "*ExtHPL[", Style["a", "TI"], ", ", Style["x", "TI"], "], {", Style["x", "TI"], ", 0, ", Style["t", "TI"], "}]",
	" integrates the integrand with respect to ", Style["x", "TI"], " on the interval [0,", Style["t", "TI"],  "], if ", Style["exp", "TI"], " is a polynomial in the letters and does not contain a constant term. \n", 
	"It can also integrate sums and lists of ", Style["exp", "TI"], "*ExtHPL[", Style["a", "TI"], ", ", Style["x", "TI"], "]. \n",
	"This routine will set divergent terms to 0."
}//StringJoin;


(*
ExtHPLAnalyticContinuationSign::usage = "Sets the sign of the imaginary part in the analytic continuation of the logarithm to negative values.
The default value is 1."
ExtHPLConvertToSimplerArgument::usage = "ExtHPLConvertToSimplerArgument[ExtHPL[y(x)],x] will convert the ExtHPL of the complicated argument y into ExtHPLs with argument x."
*)
ExtHPLAnalyticContinuationSign::usage = ToString[#, StandardForm]&/@{
	"ExtHPLAnalyticContinuationSign sets the sign convention of the analytic continuation of the HPLs. \n",
	"The default value is 1."
}//StringJoin;
ExtHPLConvertToSimplerArgument::usage = ToString[#, StandardForm]&/@{
	"ExtHPLConvertToSimplerArgument[ExtHPL[", Style["a", "TI"], ", ", Style["y","TI"], "(", Style["x", "TI"], ")], ", Style["x", "TI"], ", ", Style["d", "TI"], "]",
	" expresses ExtHPL[", Style["a", "TI"], ", ", Style["y","TI"], "(", Style["x", "TI"], ")] in terms of HPLs with the argument ", Style["x","TI"], ", \n",
	"The arising HPLs at unity will not be evaluated. \n",
	"If ", Style["d", "TI"], " is omitted, is is assumend to be 1. \n",
	"The transformations ", Style["y","TI"], "(", Style["x", "TI"], ")=1-", Style["x", "TI"], " and ", Style["y","TI"], "(", Style["x", "TI"], ")=1/", Style["x", "TI"], " are implemented separately for ", Style["d", "TI"], "=1."
}//StringJoin;


ExtHPLValueTable::usage = ToString[#, StandardForm]&/@{
	"Dispatch table of rules, with the values of all ExtHPLs up to weight 5."
}//StringJoin;
ExtHPLEvaluate::usage = ToString[#, StandardForm]&/@{
	"ExtHPLEvaluate[", Style["exp", "TI"], "]",
	" replaces all ExtHPLs at 1 up to weight 5 with its numerical value. \n",
	"It uses the dispatch table ", Style["ExtHPLValueTable", "TI"], "."
}//StringJoin;


(* ::Subsection::Closed:: *)
(*Begin Private*)


Begin["Private`"];


(* ::Subsection::Closed:: *)
(*Basic Definitions*)


$ExtHPLPath=FileNameJoin[Drop[FileNameSplit[$InputFileName],-1]];


ExtHPLLetter[sign_,x_]:=Switch[sign,
	"0",1/x,
	"1",1/(1-x),
	"-1",1/(1+x),
	"{4,0}",1/(1+x^2),
	"{4,1}",x/(1+x^2),
	_,customExtHPLLetter[sign,x]
];


LeibnizRule[a_?AtomQ,operator_Function]:=operator[a];
LeibnizRule[a_,operator_Function]:=Plus@@Table[MapAt[operator,a,n],{n,Length[a]}];


(*ExtHPL[a_?AtomQ,x_]:=ExtHPL[{a},x];*)
ExtHPL[a:{__?((AtomQ[#]||ListQ[#])&&!StringQ[#]&)},x_]:=ExtHPL[ToString/@a,x]/;ContainsOnly[a,{0,1,-1,{4,0},{4,1}}];
ExtHPL[{},x_]:=1;
ExtHPLDerivative[ExtHPL[a:{__?AtomQ},x_],x_]:=ExtHPLLetter[First[a],x]*ExtHPL[Drop[a,1],x];
ExtHPLDerivative[a_List,x_]:=ExtHPLDerivative[#,x]&/@a;
ExtHPLDerivative[a_Plus,x_]:=ExtHPLDerivative[#,x]&/@a;
ExtHPLDerivative[a_Times,x_]:=LeibnizRule[a,ExtHPLDerivative[#,x]&];
ExtHPLDerivative[a_?(FreeQ[ExtHPL]),x_]:=D[a,x];


If[ValueQ[ExtHPLAnalyticContinuationSign],Null,ExtHPLAnalyticContinuationSign:=1];


(* ::Subsection::Closed:: *)
(*Extract singularities by shuffling / Remove leading and trailing letters*)


ExtHPLConvertToKnownFunction[ExtHPL[{b_},x_]]:=Switch[b,
	"0",Log[x],
	"1",-Log[1-x],
	"-1",Log[1+x],
	"{4,0}",ArcTan[x],
	"{4,1}",Log[1+x^2]/2
];

Options[ExtHPLRemoveTrailingLetters]={"AutoConvertToKnownFunctions"->True};
ExtHPLRemoveTrailingLetters[ExtHPL[a:{__?AtomQ},x_],b_?IntegerQ,OptionsPattern[ExtHPLRemoveTrailingLetters]]:=ExtHPLRemoveTrailingLetters[ExtHPL[a,x],ToString[b],"AutoConvertToKnownFunctions"->OptionValue["AutoConvertToKnownFunctions"]];
ExtHPLRemoveTrailingLetters[ExtHPL[a:{__?AtomQ},x_],b_?StringQ,OptionsPattern[ExtHPLRemoveTrailingLetters]]:=ExtHPL[a,x]/; Last[a]!=b;
ExtHPLRemoveTrailingLetters[ExtHPL[a:{__?AtomQ},x_],b_?StringQ,OptionsPattern[ExtHPLRemoveTrailingLetters]]:=If[OptionValue["AutoConvertToKnownFunctions"],
	1/Length[a]!*(ExtHPLConvertToKnownFunction[ExtHPL[{b},x]])^Length[a],
	1/Length[a]!*(ExtHPL[{b},x])^Length[a]
]/; AllTrue[a, # == b &];
ExtHPLRemoveTrailingLetters[ExtHPL[a:{__?AtomQ,b_?StringQ},x_],b_?StringQ,OptionsPattern[ExtHPLRemoveTrailingLetters]]:=Module[{table,count,singularity},
	table = Table[Insert[Drop[a,-1],b,i],{i,Length[a]}];
	count = Count[table,a];
	table = Select[table,FreeQ[a]];
	singularity = If[OptionValue["AutoConvertToKnownFunctions"],ExtHPLConvertToKnownFunction[ExtHPL[{b},x]],ExtHPL[{b},x]];
	(singularity*ExtHPLRemoveTrailingLetters[ExtHPL[Drop[a,-1],x],b,"AutoConvertToKnownFunctions"->OptionValue["AutoConvertToKnownFunctions"]]-Plus@@(ExtHPLRemoveTrailingLetters[ExtHPL[#,x],b,"AutoConvertToKnownFunctions"->OptionValue["AutoConvertToKnownFunctions"]]&/@table))/count
]/; AnyTrue[a, # != b &];
ExtHPLRemoveTrailingLetters[exp_,b_?StringQ,OptionsPattern[ExtHPLRemoveLeadingLetters]]:=If[FreeQ[exp,ExtHPL],exp,
	exp/.{ExtHPL[a:{__?AtomQ},y_]:>ExtHPLRemoveTrailingLetters[ExtHPL[a,y],b,"AutoConvertToKnownFunctions"->OptionValue["AutoConvertToKnownFunctions"]]}
]/;Head[exp]=!=ExtHPL;

Options[ExtHPLRemoveLeadingLetters]={"AutoConvertToKnownFunctions"->True};
ExtHPLRemoveLeadingLetters[exp_,b_?IntegerQ,OptionsPattern[ExtHPLRemoveLeadingLetters]]:=ExtHPLRemoveLeadingLetters[exp,ToString[b],"AutoConvertToKnownFunctions"->OptionValue["AutoConvertToKnownFunctions"]];
ExtHPLRemoveLeadingLetters[ExtHPL[a:{__?AtomQ},x_],b_?StringQ,OptionsPattern[ExtHPLRemoveLeadingLetters]]:=ExtHPL[a,x]/; First[a]!=b;
ExtHPLRemoveLeadingLetters[ExtHPL[a:{__?AtomQ},x_],b_?StringQ,OptionsPattern[ExtHPLRemoveLeadingLetters]]:=1/Length[a]!*(ExtHPL[{b},x])^Length[a]/; AllTrue[a, # == b &];
ExtHPLRemoveLeadingLetters[ExtHPL[a:{b_?StringQ,__?AtomQ},x_],b_?StringQ,OptionsPattern[ExtHPLRemoveLeadingLetters]]:=Module[{table,count,singularity},
	table = Table[Insert[Drop[a,1],b,i],{i,Length[a]}];
	count = Count[table,a];
	table = Select[table,FreeQ[a]];
	singularity = If[OptionValue["AutoConvertToKnownFunctions"],ExtHPLConvertToKnownFunction[ExtHPL[{b},x]],ExtHPL[{b},x]];
	(singularity*ExtHPLRemoveLeadingLetters[ExtHPL[Drop[a,1],x],b,"AutoConvertToKnownFunctions"->OptionValue["AutoConvertToKnownFunctions"]]-Plus@@(ExtHPLRemoveLeadingLetters[ExtHPL[#,x],b,"AutoConvertToKnownFunctions"->OptionValue["AutoConvertToKnownFunctions"]]&/@table))/count
]/; AnyTrue[a, # != b &];
ExtHPLRemoveLeadingLetters[exp_,b_?StringQ,OptionsPattern[ExtHPLRemoveLeadingLetters]]:=If[FreeQ[exp,ExtHPL],exp,
	exp/.{ExtHPL[a:{__?AtomQ},y_]:>ExtHPLRemoveLeadingLetters[ExtHPL[a,y],b,"AutoConvertToKnownFunctions"->OptionValue["AutoConvertToKnownFunctions"]]}
]/;Head[exp]=!=ExtHPL;


(* ::Subsection::Closed:: *)
(*Series Expansion*)


(* ::Text:: *)
(*Series Expansion of ExtHPLs around zero rewriting of trailing zeros with the shuffle algebra*)


ExtHPLSeries[exp_?(FreeQ[ExtHPL]),{x_,x0_,n_}]:=Normal[Series[exp,{x,x0,n}]];
ExtHPLSeries[exp_Plus,{x_,x0_,n_}]:=Collect[ExtHPLSeries[#,{x,x0,n}]&/@(exp),x];
ExtHPLSeries[ExtHPL[a:{"0"..},x_],{x_,0,n_}]:=Log[x]^Length[a]/(Length[a]!);
ExtHPLSeries[ExtHPL[a:{__?AtomQ,"0"},x_],{x_,0,n_}]:=Collect[Collect[ExtHPLRemoveTrailingLetters[ExtHPL[a,x],"0"],ExtHPL[c_,x]]/.(ExtHPL[b:{__?AtomQ},x]:>ExtHPLSeries[ExtHPL[b, x],{x, 0, n}]),x];
ExtHPLSeries[ExtHPL[a:{__?AtomQ},x_],{x_,0,n_}]:=Module[{Z=Table[Indexed[coeff,i],{i,n}]},
	For[i=1,i<Length[a],i++,
		Switch[a[[i]],
			"0",Z=Z/.Indexed[coeff,j_]:>1/j*Indexed[coeff,j],
			"1",Z=Z/.Indexed[coeff,j_]:>1/j*Sum[Indexed[coeff,k],{k,j-1}],
			"-1",Z=Z/.Indexed[coeff,j_]:>(-1)^j/j*Sum[(-1)^(k+1)*Indexed[coeff,k],{k,j-1}],
			"{4,0}",Z=Z/.Indexed[coeff,j_]:>1/j*Sum[(-1)^((j-k-1)/2)*Indexed[coeff,k],{k,Mod[j,2]+1,j-1,2}],
			"{4,1}",Z=Z/.Indexed[coeff,j_]:>1/j*Sum[(-1)^((j-k)/2-1)*Indexed[coeff,k],{k,Mod[j-1,2]+1,j-2,2}]
		];
	];
	Switch[Last[a],
		"1",Z=Z/.Indexed[coeff,j_]:>1/j,
		"-1",Z=Z/.Indexed[coeff,j_]:>-1*(-1)^j/j,
		"{4,0}",Z=Z/.Indexed[coeff,j_]:>If[EvenQ[j],0,(-1)^((j-1)/2)/j],
		"{4,1}",Z=Z/.Indexed[coeff,j_]:>If[EvenQ[j],(-1)^(j/2+1)/j,0],
		"0",Throw["error"]
	];
	Sum[Z[[i]]*x^i,{i,n}]
]/;Last[a]!="0";
ExtHPLSeries[exp_?(FreeQ[ExtHPL])*ExtHPL[a:{__?AtomQ},x_],{x_,0,n_}]:=Module[{series},
	series=Normal[Series[exp,{x,0,n}]];
	Collect[Expand[series*ExtHPLSeries[ExtHPL[a,x],{x,0,n-Min[0,Exponent[series,x,Min]]}]],t]/.{x^m_?(#>n&)->0}
];

ExtHPLSeriesCoefficient[exp_?(FreeQ[ExtHPL]),{x_,x0_,n_}]:=SeriesCoefficient[exp,{x,x0,n}];
ExtHPLSeriesCoefficient[exp_Plus,{x_,x0_,n_}]:=Collect[ExtHPLSeriesCoefficient[#,{x,x0,n}]&/@(exp),x];
ExtHPLSeriesCoefficient[ExtHPL[a:{"0"..},x_],{x_,0,0}]:=Log[x]^Length[a]/(Length[a]!);
ExtHPLSeriesCoefficient[ExtHPL[a:{__?AtomQ},x_],{x_,0,0}]:=0/;Last[a]!="0";
ExtHPLSeriesCoefficient[ExtHPL[a:{"0"..},x_],{x_,0,n_?(#>0&&IntegerQ[#]&)}]:=0;
ExtHPLSeriesCoefficient[ExtHPL[a:{__?AtomQ,"0"},x_],{x_,0,n_}]:=Collect[ExtHPLRemoveTrailingLetters[ExtHPL[a,x],"0"],ExtHPL[c_,x]]/.(ExtHPL[b:{__?AtomQ},x]:>ExtHPLSeriesCoefficient[ExtHPL[b, x],{x, 0, n}]);
ExtHPLSeriesCoefficient[ExtHPL[a:{__?AtomQ},x_],{x_,0,n_}]:=Module[{Z=Table[Indexed[coeff,i],{i,n}]},
	For[i=1,i<Length[a],i++,
		Switch[a[[i]],
			"0",Z=Z/.Indexed[coeff,j_]:>1/j*Indexed[coeff,j],
			"1",Z=Z/.Indexed[coeff,j_]:>1/j*Sum[Indexed[coeff,k],{k,j-1}],
			"-1",Z=Z/.Indexed[coeff,j_]:>(-1)^j/j*Sum[(-1)^(k+1)*Indexed[coeff,k],{k,j-1}],
			"{4,0}",Z=Z/.Indexed[coeff,j_]:>1/j*Sum[(-1)^((j-k-1)/2)*Indexed[coeff,k],{k,Mod[j,2]+1,j-1,2}],
			"{4,1}",Z=Z/.Indexed[coeff,j_]:>1/j*Sum[(-1)^((j-k)/2-1)*Indexed[coeff,k],{k,Mod[j-1,2]+1,j-2,2}]
		];
	];
	Switch[Last[a],
		"1",Z[[n]]/.Indexed[coeff,j_]:>1/j,
		"-1",Z[[n]]/.Indexed[coeff,j_]:>-1*(-1)^j/j,
		"{4,0}",Z[[n]]/.Indexed[coeff,j_]:>If[EvenQ[j],0,(-1)^((j-1)/2)/j],
		"{4,1}",Z[[n]]/.Indexed[coeff,j_]:>If[EvenQ[j],(-1)^(j/2+1)/j,0]
	]
]/;(Last[a]!="0"&&n!=0);


(* ::Text:: *)
(*Quick Implementation of the Product of one HPL with another of weight 1. (Formula used in the Series expansion to get rid of trailing zeros ):*)


(* 
ExtHPL /: ExtHPL[a:{__?AtomQ},x_]^n_?(IntegerQ[#]&&#>1&):=Product[ExtHPL[a,x],{i,n}];
ExtHPL /: ExtHPL[a:{__?AtomQ},x_]*ExtHPL[{b_?AtomQ},x_]:=Plus@@(ExtHPL[#,x]&/@Table[Insert[a,b,i],{i,Length[a]+1}]);
*)


(* ::Subsection::Closed:: *)
(*Integration*)


(* ::Text:: *)
(*Integration of ExtHPLs *)


ExtHPLIntegrate[f_,{y_,s_,t_}]:=Module[{res},
	res=ExtHPLIntegrate[f,{y,0,t}];
	res-(res/.{t->s})
];
ExtHPLIntegrate[f_,{y_,0,t_}]:=Block[{integrate, ExtHPLLetterToXFun, ExpToXFun, XFunToExp, ExtHPLProductLetters, ExtHPLProductLettersRules, afun, bfun, cfun, dfun, efun},
	integrate[exp_List,x_]:=integrate[#,x]&/@(exp);
	integrate[exp_Plus,x_]:=integrate[#,x]&/@(exp);
	integrate[exp_Plus*ExtHPL[a : {__?AtomQ}, x_],x_]:=integrate[Expand[exp*ExtHPL[a,x]],x];
	integrate[a_*exp_,x_]:=a*integrate[exp,x]/;(FreeQ[a,x]&&FreeQ[a,afun]&&FreeQ[a,bfun]&&FreeQ[a,cfun]&&FreeQ[a,dfun]&&FreeQ[a,efun]);
	
	ExtHPLLetterToXFun[a_?AtomQ]:=Switch[a,
		"0",afun,
		"1",bfun,
		"-1",cfun,
		"{4,0}",dfun,
		"{4,1}",efun
	];
	
	(* Products of letters *)
	ExtHPLProductLettersRules:={
	(* 2 letters *)
	afun*bfun:>bfun+afun,
	afun*cfun:>afun-cfun,
	afun*dfun:>afun-efun,
	afun*efun:>dfun,
	bfun*cfun:>(cfun+bfun)/2,
	bfun*dfun:>(bfun+dfun+efun)/2,
	bfun*efun:>(bfun-dfun+efun)/2,
	cfun*dfun:>(cfun+dfun-efun)/2,
	cfun*efun:>(-cfun+dfun+efun)/2,
	
	(* powers of letters *)
	afun*bfun^n_?IntegerQ:>Sum[bfun^i,{i,1,n}]+afun,
	afun*cfun^n_?IntegerQ:>-Sum[cfun^i,{i,1,n}]+afun,
	afun*dfun^n_?IntegerQ:>afun-Sum[efun*dfun^i,{i,0,n-1}],
	bfun*afun^n_?IntegerQ:>Sum[afun^i,{i,1,n}]+bfun,
	bfun*cfun^n_?IntegerQ:>Sum[cfun^i/2^(n-i+1),{i,1,n}]+bfun/2^n,
	bfun*dfun^n_?IntegerQ:>Sum[(dfun^i+efun*dfun^(i-1))/2^(n-i+1),{i,1,n}]+bfun/2^n,
	cfun*afun^n_?IntegerQ:>Sum[(-1)^(n+i)*afun^i,{i,1,n}]+(-1)^n*cfun,
	cfun*bfun^n_?IntegerQ:>Sum[bfun^i/2^(n-i+1),{i,1,n}]+cfun/2^n,
	cfun*dfun^n_?IntegerQ:>Sum[(dfun^i-efun*dfun^(i-1))/2^(n-i+1),{i,1,n}]+cfun/2^n,
	dfun*afun^n_?IntegerQ:>(-1)^((n+Mod[n,2])/2)*If[Mod[n,2]==1,efun,dfun]+Sum[(-1)^((n-i)/2)*afun^i,{i,n,1,-2}],
	dfun*bfun^n_?(IntegerQ[#]&&#>1&):>(bfun^n+2*bfun^(n-1)*dfun-bfun^(n-2)*dfun)/2, (* maybe solve recursion *)
	dfun*cfun^n_?(IntegerQ[#]&&#>1&):>(cfun^n+2*cfun^(n-1)*dfun-cfun^(n-2)*dfun)/2, (* maybe solve recursion *)
	efun*afun^n_?IntegerQ:>dfun*afun^(n-1),
	efun*bfun^n_?IntegerQ:>dfun*bfun^n-dfun*bfun^(n-1),
	efun*cfun^n_?IntegerQ:>dfun*cfun^(n-1)-dfun*cfun^n,
	afun^2*dfun^n_?IntegerQ:>afun^2-Sum[dfun^i,{i,1,n}],
	
	(* powers of efun *)
	efun^a_?(#>1&&IntegerQ[#]&):>(dfun-dfun^2)*efun^(a-2) (* maybe solve recursion *)
	};
	ExtHPLProductLetters[exp_]:=Module[{temp},
		temp=Expand[exp]//.ExtHPLProductLettersRules;
		If[temp===exp,exp,ExtHPLProductLetters[temp]]
	];
	
	(*
	(* Multiplication between 2 letters *)
	afun /: afun*bfun:=bfun+afun;
	afun /: afun*cfun:=afun-cfun;
	afun /: afun*dfun:=afun-efun;
	afun /: afun*efun:=dfun;
	bfun /: bfun*cfun:=(cfun+bfun)/2;
	bfun /: bfun*dfun:=(bfun+dfun+efun)/2;
	bfun /: bfun*efun:=(bfun-dfun+efun)/2;
	cfun /: cfun*dfun:=(cfun+dfun-efun)/2;
	cfun /: cfun*efun:=(-cfun+dfun+efun)/2;
	
	(* Multiplication between powers of letters *)
	afun /: afun*bfun^n_?IntegerQ:=Sum[bfun^i,{i,1,n}]+afun;
	afun /: afun*cfun^n_?IntegerQ:=-Sum[cfun^i,{i,1,n}]+afun;
	afun /: afun*dfun^n_?IntegerQ:=afun-Sum[efun*dfun^i,{i,0,n-1}];
	bfun /: bfun*afun^n_?IntegerQ:=Sum[afun^i,{i,1,n}]+bfun;
	bfun /: bfun*cfun^n_?IntegerQ:=Sum[cfun^i/2^(n-i+1),{i,1,n}]+bfun/2^n;
	bfun /: bfun*dfun^n_?IntegerQ:=Sum[(dfun^i+efun*dfun^(i-1))/2^(n-i+1),{i,1,n}]+bfun/2^n;
	cfun /: cfun*afun^n_?IntegerQ:=Sum[(-1)^(n+i)*afun^i,{i,1,n}]+(-1)^n*cfun;
	cfun /: cfun*bfun^n_?IntegerQ:=Sum[bfun^i/2^(n-i+1),{i,1,n}]+cfun/2^n;
	cfun /: cfun*dfun^n_?IntegerQ:=Sum[(dfun^i-efun*dfun^(i-1))/2^(n-i+1),{i,1,n}]+cfun/2^n;
	dfun /: dfun*afun^n_?IntegerQ:=(-1)^((n+Mod[n,2])/2)*If[Mod[n,2]==1,efun,dfun]+Sum[(-1)^((n-i)/2)*afun^i,{i,n,1,-2}];
	dfun /: dfun*bfun^n_?(IntegerQ[#]&&#>1&):=(bfun^n+2*bfun^(n-1)*dfun-bfun^(n-2)*dfun)/2; (* maybe solve recursion *)
	dfun /: dfun*cfun^n_?(IntegerQ[#]&&#>1&):=(cfun^n+2*cfun^(n-1)*dfun-cfun^(n-2)*dfun)/2; (* maybe solve recursion *)
	efun /: efun*afun^n_?IntegerQ:=dfun*afun^(n-1);
	efun /: efun*bfun^n_?IntegerQ:=dfun*bfun^n-dfun*bfun^(n-1);
	efun /: efun*cfun^n_?IntegerQ:=dfun*cfun^(n-1)-dfun*cfun^n;

	(* Rewriting of powers of efun = "{4,1}" *)
	efun /: efun^a_?(#>1&&IntegerQ[#]&):=(dfun-dfun^2)*efun^(a-2); (* maybe solve recursion *)
	*)
	
	(* Convert the expression into sums and powers of letters  *)
	ExpToXFun[exp_,x_]:=Apart[exp,x]/.{
		x^a_?(Negative[#]&&IntegerQ[#]&)->afun^(-a),
		
		(1-x)^a_?(Negative[#]&&IntegerQ[#]&)->bfun^(-a),
		(-x+1)^a_?(Negative[#]&&IntegerQ[#]&)->bfun^(-a),
		(x-1)^a_?(Negative[#]&&IntegerQ[#]&)->(-bfun)^(-a),
		(-1+x)^a_?(Negative[#]&&IntegerQ[#]&)->(-bfun)^(-a),
		
		(1+x)^a_?(Negative[#]&&IntegerQ[#]&)->cfun^(-a),
		(x+1)^a_?(Negative[#]&&IntegerQ[#]&)->cfun^(-a),
		
		(1+x^2)^a_?(Negative[#]&&IntegerQ[#]&)->dfun^(-a),
		(x^2+1)^a_?(Negative[#]&&IntegerQ[#]&)->dfun^(-a)
	}/.{
		exp2_*dfun:>Expand[exp2*dfun],
		exp2_*dfun^a_:>Expand[exp2*dfun^a]
	}/.{
		x*dfun->efun,
		x*dfun^a_?(Positive[#]&&IntegerQ[#]&)->efun*dfun^(a-1)
	};
	
	(* Convert letters into functions of the variable x *)
	XFunToExp[exp_,x_]:=exp/.{afun->1/x, bfun->1/(1-x), cfun->1/(1+x), dfun->1/(1+x^2), efun->x/(1+x^2)};
	
	(* Integrate letters *)
	integrate[exp_,x_] := XFunToExp[exp,x]*x/;(FreeQ[exp,x]&&FreeQ[exp,afun]&&FreeQ[exp,bfun]&&FreeQ[exp,cfun]&&FreeQ[exp,dfun]&&FreeQ[exp,efun]);
	integrate[afun, x_] := ExtHPL[{"0"}, x];
	integrate[bfun, x_] := ExtHPL[{"1"}, x];
	integrate[cfun, x_] := ExtHPL[{"-1"}, x];
	integrate[dfun, x_] := ExtHPL[{"{4,0}"}, x];
	integrate[efun, x_] := ExtHPL[{"{4,1}"}, x];
	
	(* Integrate powers of letters *)
	integrate[afun^a_?(#!=1&), x_] := x^(1-a)/(1-a);
	integrate[bfun^a_?(#!=1&), x_] := ((1-x)^(1-a)-1)/(a-1);
	integrate[cfun^a_?(#!=1&), x_] := ((1+x)^(1-a)-1)/(1-a);
	integrate[dfun^a_?(IntegerQ[#] && # > 1 &), x_] := (1+1/(2*(1-a)))*integrate[dfun^(a-1),x]-x*(1+x^2)^(1-a)/(2*(1-a)); (* solve recursion *)
	integrate[efun*dfun, x_] := -(1/(1+x^2)-1)/2;
	integrate[efun*dfun^a_?(#!=0&), x_] := ((1+x^2)^(-a)-1)/(2*(-a));
	
	(* Integrate letters * ExtHPL *)
	integrate[afun*ExtHPL[a : {__?AtomQ}, x_], x_] := ExtHPL[Prepend[a, "0"], x];
	integrate[bfun*ExtHPL[a : {__?AtomQ}, x_], x_] := ExtHPL[Prepend[a, "1"], x];
	integrate[cfun*ExtHPL[a : {__?AtomQ}, x_], x_] := ExtHPL[Prepend[a, "-1"], x];
	integrate[dfun*ExtHPL[a : {__?AtomQ}, x_], x_] := ExtHPL[Prepend[a, "{4,0}"], x];
	integrate[efun*ExtHPL[a : {__?AtomQ}, x_], x_] := ExtHPL[Prepend[a, "{4,1}"], x];
	
	(* Integrate only ExtHPL *)
	integrate[ExtHPL[a : {__?AtomQ}, x_], x_]:=x*ExtHPL[a,x]-integrate[Expand[ ExpToXFun[x*XFunToExp[ExtHPLLetterToXFun[First[a]],x],x] *ExtHPL[Drop[a, 1], x]],x];
	(* Integrate powers of letters * ExtHPL *)
	integrate[efun*dfun*ExtHPL[a : {__?AtomQ}, x_], x_] := -1/2/(1+x^2)*ExtHPL[a,x]
		+1/2*integrate[Expand[ExtHPLProductLetters[dfun*ExtHPLLetterToXFun[First[a]]]*ExtHPL[Drop[a, 1], x]],x];
	integrate[afun^n_?(IntegerQ[#] && # > 1 &)*ExtHPL[a : {__?AtomQ}, x_], x_] := 
		-1/(n - 1)/x^(n - 1)*ExtHPL[a, x] + 1/(n-1)*(ExtHPLSeriesCoefficient[ExtHPL[a,x],{x,0,n-1}]/.{Log[x]->0})
		+1/(n - 1)*integrate[Expand[ExtHPLProductLetters[afun^(n - 1)*ExtHPLLetterToXFun[First[a]]]*ExtHPL[Drop[a, 1], x]], x];
	integrate[bfun^n_?(IntegerQ[#] && # > 1 &)*ExtHPL[a : {__?AtomQ}, x_], x_] := 1/(n - 1)/(1 - x)^(n - 1)*ExtHPL[a, x]
		-1/(n - 1)*integrate[Expand[ExtHPLProductLetters[bfun^(n - 1)*ExtHPLLetterToXFun[First[a]]]*ExtHPL[Drop[a, 1], x]], x];
	integrate[cfun^n_?(IntegerQ[#] && # > 1 &)*ExtHPL[a : {__?AtomQ}, x_], x_] := -1/(n - 1)/(1 + x)^(n - 1)*ExtHPL[a, x]
		+1/(n - 1)*integrate[Expand[ExtHPLProductLetters[cfun^(n - 1)*ExtHPLLetterToXFun[First[a]]]*ExtHPL[Drop[a, 1], x]], x];
	(* There is a mistake inside this line
	integrate[dfun^n_?(IntegerQ[#] && # > 1 &)*ExtHPL[a : {__?AtomQ}, x_], x_] := 1/(2*(1-n))*(1+x^2)^(1-n)/x*ExtHPL[a,x]+1/(2*(n-1))*(
		-integrate[afun^2*ExtHPL[a,x],x]+integrate[Expand[afun*ExtHPLLetterToXFun[First[a]]*ExtHPL[Drop[a, 1], x]],x]
		+Sum[integrate[dfun^i*ExtHPL[a,x],x] - integrate[Expand[efun*dfun^(i-1)*ExtHPLLetterToXFun[First[a]]*ExtHPL[Drop[a, 1], x]],x],{i,1,n-1}]); *)
	integrate[dfun^n_?(IntegerQ[#] && # > 1 &)*ExtHPL[a : {__?AtomQ}, x_], x_] := 1/(2*(1-n))*(1+x^2)^(1-n)/x*ExtHPL[a,x] - 1/(2*(1-n))*(ExtHPLSeriesCoefficient[ExtHPL[a,x],{x,0,1}]/.{Log[x]->0})
		+ 1/(2*(n-1))*( integrate[ Expand[ExtHPLProductLetters[(Sum[dfun^i,{i,1,n-1}]-afun^2)]*ExtHPL[a,x] ] + Expand[ ExtHPLProductLetters[ExtHPLLetterToXFun[First[a]]*dfun^(n-1)*afun]*ExtHPL[Drop[a,1],x] ],x] );
	
	integrate[efun*dfun^n_?(IntegerQ[#] && # > 0 &)*ExtHPL[a : {__?AtomQ}, x_], x_] := -1/(2n)*(1+x^2)^(-n)*ExtHPL[a,x]
		+1/(2*n)*integrate[Expand[ExtHPLProductLetters[dfun^n*ExtHPLLetterToXFun[First[a]]]*ExtHPL[Drop[a, 1], x]],x];
	
	(* Rewrite expression in terms of letters or hold, if not possible *)
	(* integrate[ExtHPL[a:{__?AtomQ},x_],x_]:=With[{temp2=ExtHPL[a,x$]},Hold[ExtHPLIntegrate[temp2,{x$,0,x}]]/.{x$->Unique[x]}]; *)
	integrate[exp_?(FreeQ[ExtHPL]),x_]:=Module[{temp},
		temp=ExpToXFun[exp, x];
		If[FreeQ[temp,x],
			integrate[temp,x],
			With[{temp2=XFunToExp[exp,x]/.{x->x$}},Hold[ExtHPLIntegrate[temp2,{x$,0,x}]]/.{x$->Unique[x]}]
		]
	]/;!(FreeQ[exp, x]);
	integrate[exp_?(FreeQ[ExtHPL])*ExtHPL[a:{__?AtomQ},x_],x_]:=Module[{temp},
		temp=ExpToXFun[exp, x];
		If[FreeQ[temp,x],
			integrate[Expand[temp*ExtHPL[a, x]],x],
			With[{temp2=(Expand[XFunToExp[exp,x]*ExtHPL[a,x]])/.{x->x$}},Hold[ExtHPLIntegrate[temp2,{x$,0,x}]]/.{x$->Unique[x]}]
		]
	]/;!(FreeQ[exp, x]);
	
	(* Old Routine without Hold
	integrate[exp_?(FreeQ[ExtHPL]),x_]:=integrate[ExpToXFun[exp, x],x]/;!(FreeQ[exp, x]);
	integrate[exp_?(FreeQ[ExtHPL])*ExtHPL[a:{__?AtomQ},x_],x_]:=integrate[Expand[ExpToXFun[exp, x]*ExtHPL[a, x]],x]/;!(FreeQ[exp, x]);
	*)
	
	(*
	XFunToFunRules:={afun->1/y,bfun->1/(1-y),cfun->1/(1+y)};
	integrate[exp_?(FreeQ[ExtHPL]),x_]:=Integrate[exp/.XFunToFunRules,x]/.{Log[y]->ExtHPL[{"0"},y],-Log[1-y]->ExtHPL[{"1"},y],Log[1+y]->ExtHPL[{"-1"},y]}; 
	*)       (* change this a little bit  to get the constant right   be aware of divergences=1/x *)
	
	integrate[Expand[f],y]/.y:>t
];


(* ::Subsection:: *)
(*Convert To Simpler Argument*)


ExtHPLConvertToSimplerArgument[1,x_,evPoint_]:=1;
ExtHPLConvertToSimplerArgument[exp_,x_]:=ExtHPLConvertToSimplerArgument[exp,x,1];

ExtHPLConvertToSimplerArgument[ExtHPL[a:{__?AtomQ},y_],x_,evPoint_]:=Module[{res},
	res=ExtHPLIntegrate[D[y,x]*ExtHPLLetter[First[a],y]
		*ExtHPLConvertToSimplerArgument[ExtHPL[Drop[a,1],y],x,evPoint],{x,0,x}];
	res-((res-ExtHPL[a,y])/.{x->evPoint})
]/;((y=!=1/x && y=!=1-x) || evPoint!=1);

ExtHPLConvertToSimplerArgument[ExtHPL[a:{__?AtomQ},1/x_],x_,1]:=Module[{res, temp1, temp2},
	res=ExtHPLIntegrate[(-1/x^2)*ExtHPLLetter[First[a],1/x]
		*ExtHPLConvertToSimplerArgument[ExtHPL[Drop[a,1],1/x],x,1],{x,0,x}];
	temp1=ExtHPLRemoveLeadingLetters[res,"1","AutoConvertToKnownFunctions"->False];
	temp2=ExtHPLRemoveLeadingLetters[ExtHPL[a,1/x],"1","AutoConvertToKnownFunctions"->False];
	res-(Expand[(temp1-(temp2/.{ExtHPL[{"1"},1/x]->(ExtHPL[{"1"},x]+ExtHPL[{"0"},x]-ExtHPLAnalyticContinuationSign*I*Pi)}))]/.{x->1})
];

ExtHPLConvertToSimplerArgument[ExtHPL[a:{__?AtomQ},1-x_],x_,1]:=Module[{res, temp1, temp2},
	res=ExtHPLIntegrate[-1*ExtHPLLetter[First[a],1-x]
		*ExtHPLConvertToSimplerArgument[ExtHPL[Drop[a,1],1-x],x,1],{x,0,x}];
	(* Make divergences at Log[x] and Log[1-x] explicit *)
	temp1=ExtHPLRemoveTrailingLetters[ExtHPLRemoveLeadingLetters[res,"1","AutoConvertToKnownFunctions"->False],"0","AutoConvertToKnownFunctions"->False];
	(* Set Log[1] to zero and describe the infinities *)
	temp1=(temp1/.{ExtHPL[{"0"},x]->0})/.{ExtHPL[{"1"},x]->-infty};
	(* Make divergences at Log[x] and Log[1-x] explicit *)
	temp2=ExtHPLRemoveTrailingLetters[ExtHPLRemoveLeadingLetters[ExtHPL[a,1-x],"1","AutoConvertToKnownFunctions"->False],"0","AutoConvertToKnownFunctions"->False];
	(* Set Log[1] to zero and describe the infinities *)
	temp2=(temp2/.{ExtHPL[b__?(Last[#]!="0"&),1-x]->0})/.{ExtHPL[{"0"},1-x]->infty};
	
	res-(Expand[temp1-temp2]/.{x->1})
]/;ContainsOnly[a,{"0","1"}];


(*
ExtHPLConvertToSimplerArgument[ExtHPL[a:{__?AtomQ},y_],x_,evPoint_]:=Module[{res, temp},
	res=ExtHPLIntegrate[D[y,x]*(ExtHPLLetter[First[a],x]/.{x->y})
	*ExtHPLConvertToSimplerArgument[ExtHPL[Drop[a,1],y],x,evPoint],{x,0,x}];
	(*If[evPoint!=1, res-(res/.{x->evPoint})+ExtHPL[a,y/.{x->evPoint}],
		temp = ExtHPLRemoveLeadingLetters[res-ExtHPL[a,y],"1","AutoConvertToKnownFunctions"->False];
		temp
		(* res-(temp/.{x->evPoint}) *)
	]*)
	res
]/;y!=1/x;

ExtHPLConvertToSimplerArgument[ExtHPL[a:{__?AtomQ},1/x_],x_,1]:=Module[{ExtHPLConvertToSimplerArgumentWithoutConstant, res, temp1, temp2},
	ExtHPLConvertToSimplerArgumentWithoutConstant[1,y_,1]:=1;
	ExtHPLConvertToSimplerArgumentWithoutConstant[ExtHPL[b:{__?AtomQ},1/y_],y_,1]:=
		ExtHPLIntegrate[-1/y^2*ExtHPLLetter[First[b],1/y]
		*ExtHPLConvertToSimplerArgumentWithoutConstant[ExtHPL[Drop[b,1],1/y],y,1],{y,0,y}];
	res=ExtHPLConvertToSimplerArgumentWithoutConstant[ExtHPL[a,1/x],x,1];
	(* temp=ExtHPLRemoveLeadingLetters[#,"1","AutoConvertToKnownFunctions"->False]&/@{res,ExtHPL[a,1/x]}; *)
	temp1=ExtHPLRemoveLeadingLetters[res,"1","AutoConvertToKnownFunctions"->False];
	temp2=ExtHPLRemoveLeadingLetters[ExtHPL[a,1/x],"1","AutoConvertToKnownFunctions"->False];
	res-(Expand[(temp1-(temp2/.{ExtHPL[{"1"},y_]->(ExtHPL[{"1"},y]-I*Pi)}))]/.{x->1})
];
*)

(* If evpoint =1:
res-ExtHPL: divergenzen wegschuffeln, dann sollten sich Divergenzen herausk\[UDoubleDot]rzen
(oder wir haben sie explizit dastehen) und dann kann ich einsetzen. *)

(* Possible Extension: Use the Interval between 0.5 and y[0.5] and check if singularities -1,0 and 1 are included.
This corresponds to integrating over these singularities / changing the domain and getting an imaginary part.
Then corresponding substitutions can be made. *)


(* ::Subsection:: *)
(*HPLs at 1*)


ExtHPLValueTable=Dispatch[Get[FileNameJoin[{$ExtHPLPath,"ValueTable.wl"}]]];
ExtHPLEvaluate[exp_]:=exp/.ExtHPLValueTable;


(* ::Subsection::Closed:: *)
(*End*)


End[];


EndPackage[];
